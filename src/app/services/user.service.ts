// import { Injectable } from '@angular/core';
// import { Subject } from 'rxjs';
// import { User } from '../models/user';

// @Injectable({
//   providedIn: 'root'
// })
// export class UserService {
//   private users: User[] = [
//     {
//       firstName: 'James',
//       lastName: 'Smith',
//       email: 'james@smith.com',
//       drinkPreference: 'Coca',
//       hobbies: [
//         'coder',
//         'cuisiner'
//       ]
//     }
//   ];
//   userSubject = new Subject<User[]>();

//   emitUsers() {
//     this.userSubject.next(this.users.slice());
//   }

//   adUser(user: User) {
//     this.users.push(user);
//     this.emitUsers();
//   }
  
//   constructor() { }
// }
import { User } from '../models/user';
import { Subject } from 'rxjs';

export class UserService {

  private users: User[] = [
    {
      firstName: 'James',
      lastName: 'Smith',
      email: 'james@smith.com',
      drinkPreference: 'Coca',
      hobbies: [
        'coder',
        'cuisiner'
      ]
    }
  ];


  userSubject = new Subject<User[]>();

  emitUsers() {
    this.userSubject.next(this.users.slice());
  }

  addUser(user: User) {
    this.users.push(user);
    this.emitUsers();
  }
}