import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppareilService {

  appareilsSubject = new Subject<any[]>();

  private appareils: any[] =[];
  // private appareils = [
  //   {
  //     id: 1,
  //     name: 'Machine à laver',
  //     status: 'éteint'
  //   },
  //   {
  //     id: 2,
  //     name: 'Frigo',
  //     status: 'allumé'
  //   },
  //   {
  //     id: 3,
  //     name: 'Ordinateur',
  //     status: 'éteint'
  //   },
  // ];

  constructor(private httpClient: HttpClient) { }


  emitAppareilsSubject () {
    this.appareilsSubject.next(this.appareils.slice());
  }

  switchOnAll() {
    for(let appareil of this.appareils) {
      appareil.status = 'allumé';
      this.emitAppareilsSubject();
    }
  }

  switchOffAll() {
      for(let appareil of this.appareils) {
        appareil.status = 'éteint';
        this.emitAppareilsSubject();
      }
  }

  switchOnOne(i: number) {
    this.appareils[i].status = 'allumé';
    this.emitAppareilsSubject();
  }

  switchOffOne(i: number) {
      this.appareils[i].status = 'éteint';
      this.emitAppareilsSubject();
  }

  saveAppareilsToServer() {
    //console.log("saveAppareilsToServer");
    this.httpClient
        .put('https://http-client-demo-gildas-default-rtdb.firebaseio.com//appareils.json', this.appareils)
        .subscribe(
          () => {
            console.log('Enregistrement terminé')
          },
          (error) => {
            console.log('Erreur de sauvegarde !');
          }
        )
  }


  getAppareilFromServer() {
    //console.log("getAppareilFromServer");
    this.httpClient
      .get<any[]>('https://http-client-demo-gildas-default-rtdb.firebaseio.com/appareils.json')
      .subscribe(
        (response) => {
          this.appareils = response;
          //console.log(this.appareils);
          this.emitAppareilsSubject();
          console.log("Responce getAppareilFromServer");
        },
        (error) => {
          console.log('Erreur de chargement' + error);
        }
      );
  }

  getAppareilById(id: number) {
    const appareil = this.appareils.find(
      (s) => {
        return s.id === id;
      }      
    );
    return appareil;
  }

  addAppareil (name: string, status: string) {
    const appareilObject = {
      id: 0,
      name: '',
      status: ''
    };
    //console.log("addAppareil : ")
    appareilObject.name = name;
    appareilObject.status = status;
    appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;
    //console.log(appareilObject);
    this.appareils.push(appareilObject);
    this.saveAppareilsToServer();
    this.emitAppareilsSubject();
  }



}
