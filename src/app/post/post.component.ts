import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() postTitle!: string;
  @Input() postContent!: string;
  @Input() postLike!: number;
  @Input() postCreateDate!: Date;


  constructor() { }

  ngOnInit(): void {
    this.postCreateDate = new Date;
  }

  add1Like () {
    return this.postLike ++;
  }

  sup1Like () {
    return this.postLike --;
  }

}
