import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isAuth: boolean = false;
  
  posts = [
    {
      title: '1 Post',
      content: 'Content mon 1 post',  
      loveIts: 0,
    },
    {
      title: '2 Post',
      content: 'Content mon 2 post',  
      loveIts: 0,
    },
    {
      title: '3 Post',
      content: 'Content mon 3 post',  
      loveIts: 0,
    }
  ]

  constructor() {
    setTimeout(
      () => {
        this.isAuth = true;
      },4000
    )
  }

  ngOnInit() {    
  }

}
